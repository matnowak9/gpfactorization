
import java.math
import java.math.BigInteger

import NodeType.NodeType

import scala.collection.mutable
import scala.util.Random

object NodeGenerator {

  def generateNode(nodeType: NodeType, param :Char = 0) :Node = nodeType match {
    case NodeType.Add =>
      new Node(new Array[Node](2), nodeType) {
        override def getValue(m: => mutable.Map[Char, Double]): Double =
          children(0).getValue(m) + children(1).getValue(m)

        override def toString =
          "(" + children(0) + "+" + children(1) + ")"
      }

    case NodeType.Param =>
      new Node(new Array[Node](0), nodeType) {
        varName = param
        override def getValue(m: => mutable.Map[Char, Double]): Double =
          m(varName)

        override def toString =
          "(" + varName + ")"
      }

    case NodeType.Mul =>
      new Node(new Array[Node](2), nodeType) {
        override def getValue(m: => mutable.Map[Char, Double]): Double =
          children(0).getValue(m) * children(1).getValue(m)

        override def toString =
          "(" + children(0) + "*" + children(1) + ")"
      }

    case NodeType.Sqrt =>
      new Node(new Array[Node](1), nodeType) {
        override def getValue(m: => mutable.Map[Char, Double]): Double =
          Math.sqrt(children(0).getValue(m))

        override def toString =
          "sqrt(" + children(0) + ")"
      }

    case NodeType.RandPrime =>
      new Node(new Array[Node](0), nodeType) {
        randPrime = Primes.randomPrime

        override def getValue(m: => mutable.Map[Char, Double]): Double =
          randPrime

        override def toString =
          "RandPrime[" + randPrime + "]"
      }

    case NodeType.IfEq =>
      new Node(new Array[Node](4), nodeType) {
        override def getValue(m: => mutable.Map[Char, Double]): Double =
          if (children(0).getValue(m) == children(1).getValue(m))
            children(2).getValue(m)
          else
            children(3).getValue(m)

        override def toString =
          "(if " + children(0) + " == " + children(1) + " then " + children(2) + " else " + children(3) + ")"
      }

    case NodeType.IfGt =>
      new Node(new Array[Node](4), nodeType) {
        override def getValue(m: => mutable.Map[Char, Double]): Double =
          if (children(0).getValue(m) > children(1).getValue(m))
            children(2).getValue(m)
          else
            children(3).getValue(m)

        override def toString =
          "(if " + children(0) + " > " + children(1) + " then " + children(2) + " else " + children(3) + ")"
      }

    case NodeType.IfLt =>
      new Node(new Array[Node](4), nodeType) {
        override def getValue(m: => mutable.Map[Char, Double]): Double =
          if (children(0).getValue(m) < children(1).getValue(m))
            children(2).getValue(m)
          else
            children(3).getValue(m)

        override def toString =
          "(if " + children(0) + " < " + children(1) + " then " + children(2) + " else " + children(3) + ")"
      }

    case NodeType.IsPrime =>
      new Node(new Array[Node](3), nodeType) {
        override def getValue(m: => mutable.Map[Char, Double]): Double = {
          val child = children(0).getValue(m)
          if (child.isValidInt && math.BigInteger.valueOf(child.toLong).isProbablePrime(1))
            children(1).getValue(m)
          else
            children(2).getValue(m)
        }

        override def toString =
          "(ifPrime " + children(0) + " then " + children(1) + " else " + children(2) + ")"
      }

    case NodeType.SetV =>
      new Node(new Array[Node](1), nodeType) {
        override def getValue(m: => mutable.Map[Char, Double]): Double = {
          m('v') = children(0).getValue(m);
          m('v')
        }

        override def toString =
          "(v = " + children(0) + ")"
      }

    case NodeType.SetW =>
      new Node(new Array[Node](1), nodeType) {
        override def getValue(m: => mutable.Map[Char, Double]): Double = {
          m('w') = children(0).getValue(m)
          m('w')
        }

        override def toString =
          "(w = " + children(0) + ")"
      }

    case NodeType.Eval =>
      new Node(new Array[Node](2), nodeType) {
        override def getValue(m: => mutable.Map[Char, Double]): Double = {
          children(0).getValue(m)
          children(1).getValue(m)
        }

        override def toString =
          "(Eval " + children(0) + ' ' + children(1) + ")"
      }

    case NodeType.For =>
      new Node(new Array[Node](2), nodeType) {
        override def getValue(m: => mutable.Map[Char, Double]): Double = {
          val l = children(0).getValue(m)
          var i = 0
          m('i') = 0
          while (i < l - 1) {
            children(1).getValue(m)
            i += 1
            m('i') = i
          }
          children(1).getValue(m)
        }

        override def toString =
          "(For " + children(0) + " do " + children(1) + ")"
      }

    case NodeType.Mod =>
      new Node(new Array[Node](2), nodeType) {
        override def getValue(m: => mutable.Map[Char, Double]): Double =
          children(0).getValue(m) % children(1).getValue(m)  //round?
        override def toString =
          "(" + children(0) + "%" + children(1) + ")"
      }
    }
}
