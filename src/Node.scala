import NodeType.NodeType

import scala.util.Random

abstract class Node(childrenc: Array[Node], nodeTypec :NodeType) {
  var children = childrenc
  def getValue(m: => scala.collection.mutable.Map[Char,Double]): Double
  var nodeType :NodeType = nodeTypec
  var varName :Char = 0
  var randPrime :Int = 0

  def duplicate() :Node = {
    var newNode = NodeGenerator.generateNode(nodeType, varName)
    newNode.randPrime = randPrime
    for (i <- 0 until children.length)
      newNode.children(i) = children(i).duplicate()
    newNode
  }

  def treeDepth :Int = {
    var max:Int = 0
    for (e <- children) {
      max = math.max(max, e.treeDepth)
    }
    max+1
  }
  /**
      * @return (All nodes in tree; function_nodes/tree_complexity)
    */
  def treeSize(): (Int, Int) = {
    if (children.length == 0)
      (1, 0)
    else {
      var all = 1
      var funct = 1
      for (c <- children){
        val r = c.treeSize()
        all += r._1
        funct += r._2
      }
      (all, funct)
    }
  }

  def randomFunNode(): Node = {
    val s = treeSize()
    val r = Random.nextInt(s._1 - 1)
    getNthFunNode(r + 1)._2
  }

  def getNthFunNode(target :Int, cur :Int = 0) :(Int, Node) = {
    if (cur + children.length >= target)
      (target, this)
    else{
      var nc = cur + children.length
      for (c <- children if c.children.length > 0){
        val o = c.getNthFunNode(target, nc)
        if (o._1 == target)
          return o
        else nc = o._1
      }
      return (nc, null)
    }
  }
}
