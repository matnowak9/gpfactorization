import scala.util.Random

object Main {
  def main (args: Array[String]) {


//    val map = scala.collection.mutable.Map[Char,Double]()
//    map += ('x' -> 4)
//    map += ('w' -> 0)
//    map += ('v' -> 0)
//    map += ('i' -> 0)
//    map += ('u' -> 4)
//    val tree = TreeGenerator.generateTree(3, Array('x','v','w', 'i'))
//    print(tree)
//    println(" = " + tree.getValue(map))



    val upperbound = 10000

    val pool: Array[Int] = {
      var p = new Array[Int](100)
      for (i <- p.indices){
        p(i) = Primes.randomPrime
      }
      p
    }
    val findf = (p:Float) => {
      var x:Float = 0
      x = p*65+23
      x
    }
    val fitness = (node:Node) => {

      var sumFitness:Double = 0

      for(p <- pool){

        var wynik = findf(p)

        val map = scala.collection.mutable.Map[Char,Double]()
        map += ('x' -> p)
        map += ('w' -> 0)
        map += ('v' -> 0)
        map += ('i' -> 0)
        map += ('u' -> 4)

        val wynikE = node.getValue(map)

        var fitnessL: Double = 0
        val delta = math.abs(wynik-wynikE)


        if (wynikE.isNaN ) {
          fitnessL = upperbound
        }else{
          fitnessL = delta/wynik
        }


        sumFitness+=fitnessL
      }
      sumFitness

    }

    val gen1 = new GeneticAlgorithm(
      fitness,
      populationSize = 1000,
      maximumGenerations = 1000
    )



    val wynik = gen1.calculateBestIndividual




    print(wynik._1)

    println(" Fitness:  " + wynik._2)
    println()
    for (i <- 0 to 10) {
      val p = Primes.randomPrime
      val map = scala.collection.mutable.Map[Char,Double]()
      map += ('x' -> p)
      map += ('w' -> 0)
      map += ('v' -> 0)
      map += ('i' -> 0)
      map += ('u' -> 4)

      val tree = wynik._1

      var wynike =   findf(p)
      println(" " + p + " " + wynike + " " +tree.getValue(map))

    }

  }


    //  val upperbound = 100
    //
    //    var elapsedTime:Long = 0
    //    def time[R](block: => R): R = {
    //      val t0 = System.nanoTime()
    //      val result = block
    //      val t1 = System.nanoTime()
    //      elapsedTime = t1 - t0
    //      result
    //    }
    //
    //    val pool: Array[(Int, Int, Int)] = {
    //      var p = new Array[(Int, Int, Int)](25)
    //      for (i <- p.indices){
    //
    //        p(i) = Primes.complexNumber
    //
    //      }
    //      p
    //    }
    //
    //    val fitness = (node:Node) => {
    //
    //      var sumFitness:Double = 0
    //
    //
    //
    //      for(p <- pool){
    //
    //        val map = scala.collection.mutable.Map[Char,Double]()
    //        map += ('x' -> p._1)
    //        map += ('w' -> 0)
    //        map += ('v' -> 0)
    //        map += ('i' -> 0)
    //        map += ('u' -> 4)
    //
    //        val wynikE = time {node.getValue(map)}
    ////        if (elapsedTime > 10000000){
    ////          println("Elapsed time: " +  elapsedTime + "ns")
    ////          println("Bottleneck "+node.toString)
    ////        }
    //
    //        var fitnessL: Double = 0
    //
    //        if (wynikE.isNaN) {
    //          fitnessL = 0
    //        }else if (p._2 == wynikE || p._3 == wynikE) {
    //          fitnessL  = 100
    //        }else
    //        {
    //
    //          //||  ||
    //          if(wynikE>1){
    //            fitnessL += 8
    //          }
    //          if(wynikE != p._1){
    //            fitnessL += 10
    //          }
    //          if(Primes.isPrime(wynikE.toInt)){
    //            fitnessL += 10
    //          }
    //          if(wynikE < 100){
    //            fitnessL += 8
    //          }
    //
    //
    //        }
    //
    //
    //        sumFitness+=100-fitnessL
    //      }
    //      sumFitness
    //
    //    }
    //
    //    val gen1 = new GeneticAlgorithm(
    //      fitness,
    //      populationSize = 1000,
    //      maximumGenerations = 1000
    //    )
    //
    //
    //
    //    val wynik = gen1.calculateBestIndividual
    //
    //
    //
    //
    //    print(wynik._1)
    //
    //    println(" Fitness:  " + wynik._2)
    //    println()
    //
    ////    val pool: Array[(Int, Int, Int)] = {
    ////      var p = new Array[(Int, Int, Int)](25)
    ////      for (i <- p.indices){
    ////        p(i) = Primes.complexNumber
    ////      }
    ////      p
    ////    }
    //    var hitsPercent:Float = 0
    //    for (p <- pool){
    //      val map = scala.collection.mutable.Map[Char,Double]()
    //      map += ('x' -> p._1)
    //      map += ('w' -> 0)
    //      map += ('v' -> 0)
    //      map += ('i' -> 0)
    //      map += ('u' -> 4)
    //
    //      val tree = wynik._1
    //      val value = tree.getValue(map)
    //
    //      if (value == p._2 || value == p._3){
    //        hitsPercent+=1
    //      }
    //
    //    }
    //    hitsPercent /= pool.length
    //    hitsPercent *= 100
    //    println("Success rate: " + hitsPercent + "%")
    //    for (i <- 0 to 10) {
    //      val p =  pool(i)
    //
    //      val map = scala.collection.mutable.Map[Char,Double]()
    //      map += ('x' -> p._1)
    //      map += ('w' -> 0)
    //      map += ('v' -> 0)
    //      map += ('i' -> 0)
    //      map += ('u' -> 4)
    //
    //      val tree = wynik._1
    //      println(" " + p  + " " +tree.getValue(map))
    //
    //    }

//  }

}
