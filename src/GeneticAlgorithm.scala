import scala.collection.mutable
import scala.util.Random

/**
 * Created by piotrek on 30.12.15.
 */


class GeneticAlgorithm(fitness: (Node) => (Double),
                       targetFitness:Any = None,
                       populationSize: Int = 100,
                       maximumGenerations: Int = 100,
                       crossoverPercent: Int = 89,
                       reproductionPercent:Int = 10,
                       mutationPercent:Int = 1,
                       treeSize:Int = 3,
                       treeDepthLimit:Int = 17)
{
// inf > fitness >= 0 smaller is better


  private def generateRandomPopulation : Array[Node] = {

      //generate random population
      val arr = new Array[Node](populationSize)
      for (x <- 0 until populationSize){
        val tree = TreeGenerator.generateTree(treeSize, Array('x','v','w', 'i')) //generate random tree
        arr.update(x, tree)
      }
      arr

  }


  def crossover(x1: Node, x2: Node): (Node, Node) = {
    val p1 = x1.duplicate()
    val p2 = x2.duplicate()

    val a = p1.randomFunNode()
    val b = p2.randomFunNode()
    val aI = Random.nextInt(a.children.length)
    val bI = Random.nextInt(b.children.length)
    val c = a.children(aI)
    a.children(aI) = b.children(bI)
    b.children(bI) = c

    val m1 = p1.treeDepth
    val m2 = p2.treeDepth

    if (m1>treeDepthLimit && m2>treeDepthLimit){
      (x1, x2)
    }else if(m1>treeDepthLimit){
      val l = Array(x1, x2)

      (p2, l(Random.nextInt(l.length)))
    }else if (m2>treeDepthLimit){
      val l = Array(x1, x2)

      (p1, l(Random.nextInt(l.length)))
    }else{
      (p1, p2)
    }
  }

  def mutation(x: Node): Node = {
    val p = x.duplicate()
    val a = p.randomFunNode()
    val aI = Random.nextInt(a.children.length)
    val s = a.children(aI).treeSize()
    a.children(aI) = TreeGenerator.generateTree(s._2, Array('x','v','w', 'i'))
    assert(p.treeDepth <= treeDepthLimit)
    p
  }

  def calculateBestIndividual: (Node, Double) = {

    var currentPopulation = generateRandomPopulation
    var currentGeneration = 0

    var bestIndividual: (Node, Double) = null

    do {
      val (bv, newPopulation) = evaluateCurrentPopulation(currentPopulation)
      bestIndividual = bv
      currentPopulation = newPopulation

      if (currentGeneration % 20  == 0){
        println("Generation: "+ currentGeneration)
        println("Best individual: " + bestIndividual._1)
        println("Fitness: " + bestIndividual._2)
        println()
      }
      currentGeneration+=1

    } while ( currentGeneration !=maximumGenerations && bestIndividual._2 != 1  )

    bestIndividual
  }


  private def uniformSelection(popWithNormAccumulatedFitn: Array[(Node, Double)]):Node = {
    val r = scala.util.Random
    var l = r.nextDouble()

    l *= popWithNormAccumulatedFitn(populationSize-1)._2



    val R = popWithNormAccumulatedFitn.find({case (a, b) => b>l})



    //TODO switch to binary search

    R.get._1

  }

  private def evaluateCurrentPopulation(currentPopulation: Array[Node]): ((Node, Double), Array[Node]) = {
    val newPopulation: Array[Node] = new Array[Node](populationSize)

    var populationWithAdjFitness: mutable.ArrayBuffer[(Node, Double)] =  mutable.ArrayBuffer.empty[(Node, Double)]


    var sumFitness:Double = 0 //sum of fitness in current population
    for ( node <- currentPopulation ) {
      //calculate raw fitness
      val fit:Double = fitness(node)
      assert(fit>=0, "Fitness must range from 0 to inf, smaller is better")
      //calculate standardized fitness
      val std:Double = fit
      //calculate adjusted fitness
      val adj:Double = 1/(1+std)
      sumFitness += adj

      populationWithAdjFitness.+=:(node, adj)
    }
    //sort fitness descending
    populationWithAdjFitness = populationWithAdjFitness.sortBy({ case (a, b: Double) => -b } )
    val populationWithNormFitnessAcumulated: Array[(Node, Double)] = new Array[(Node, Double)](populationSize)


    var accumulation: Double = 0
    var x = 0
    //create vector of (Node, Fitness) where fitness is accumulated normalised fitness
    for ( (node, adjFitness) <- populationWithAdjFitness) {

      val localFitnessNormalized = adjFitness/sumFitness
      populationWithNormFitnessAcumulated(x) = (node, localFitnessNormalized + accumulation)
      x+=1

      accumulation += localFitnessNormalized
    }


    x = 0
    while(x < populationSize){

      val r = scala.util.Random
      val l = r.nextInt(100)

      assert(crossoverPercent+reproductionPercent+mutationPercent==100, "Must sum to 100%")
      if (l<crossoverPercent) {
        //perform crossover
        val i1 = uniformSelection(populationWithNormFitnessAcumulated)
        val i2 = uniformSelection(populationWithNormFitnessAcumulated)

        val (n1, n2) = crossover(i1,i2)
        assert(n1.treeDepth <= treeDepthLimit)
        assert(n2.treeDepth <= treeDepthLimit)

        newPopulation(x) =  n1
        x+=1
        if (x<populationSize){
          newPopulation(x) = n2
          x+=1
        }


      }else if (l<crossoverPercent+reproductionPercent) {
        //perform reproduction
        val newIndividual = uniformSelection(populationWithNormFitnessAcumulated).duplicate()
        newPopulation(x) = newIndividual
        x+=1

      }else {
        //perform mutation
        val newIndividual = uniformSelection(populationWithNormFitnessAcumulated)
        newPopulation(x) =  newIndividual
        x+=1
      }

    }

    (populationWithAdjFitness.head, newPopulation)
  }

}
