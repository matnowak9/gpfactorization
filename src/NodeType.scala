import scala.util.Random

object NodeType extends Enumeration {

  type NodeType = Value
  val Add, Mul, RandPrime, Sqrt, Param, IfEq, IfLt, IfGt, SetW, SetV, Eval, For, Mod, IsPrime = Value
//For jest wywalony
  val allowedFunctions = Array(Add, Mul, Sqrt, IfEq, IfLt, IfGt, SetW, SetV, Eval, Mod, IsPrime )
  val allowedTerminals = Array(Param, RandPrime)

  def randomValue :NodeType ={
    var seq = values.toSeq
    seq(Math.abs(Random.nextInt(seq.length)))
  }

  def randomFunction :NodeType ={
    allowedFunctions(Math.abs(Random.nextInt(allowedFunctions.length)))
  }

  def randomTerminal :NodeType ={
    allowedTerminals(Math.abs(Random.nextInt(allowedTerminals.length)))
  }
}