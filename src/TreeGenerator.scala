import java.util

import scala.util.Random

object TreeGenerator {
  def generateTree(complexity: Int, params: Array[Char]) :Node = {
    if (complexity == 0)
      NodeGenerator.generateNode(NodeType.randomTerminal, getRandomParam(params))
    else {
      val root = NodeGenerator.generateNode(NodeType.randomFunction)
      val childrenCount = root.children.length
      val childrenComplexity = Array.fill[Int](childrenCount)(0)

      var i = 0
      while (i < complexity - 1) {
        childrenComplexity(Math.abs(Random.nextInt(childrenCount))) += 1
        i+=1
      }

      i = 0
      while (i < childrenCount) {
        root.children(i) = generateTree(childrenComplexity(i), params)
        i += 1
      }
      root
    }
  }

  def getRandomParam(params :Array[Char]) :Char = {
    params(Math.abs(Random.nextInt(params.length)))
  }
}
