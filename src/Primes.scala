/**
 * Created by piotrek on 31.12.15.
 */
object Primes {
  //defines and provides access to first 100 primes
  var primes = Array(2, 3, 5, 7,  11, 13, 17, 19,  23,  29 ,
  31, 37 ,41,  43, 47,  53, 59  , 61,  67,  71,
  73 ,  79 ,   83,   89  ,   97)

  def randomPrime : Int = {
    val r = scala.util.Random
    val l = r.nextInt(primes.length)

    primes(l)

  }
  //returns complex number that is product of two primes and its two prime factors, for fitness evaluation
  def complexNumber: (Int, Int, Int) = {
     val l1 = randomPrime
    val l2 = randomPrime
    (randomPrime * randomPrime, l1, l2)
  }
  def isPrime(n:Int): Boolean = {
    for( p <- primes){
      if (p==n){
        return true
      }
    }
    false
  }
}
